import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const { BASE_URL } = process.env;
export default new Router({
  mode: "history",
  base: BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: () =>
        import(/* webpackChunkName: "home" */ "@/views/Home.vue"),
    },
    {
      path: "/survey",
      name: "survey",
      component: () =>
        import(/* webpackChunkName: "home" */ "@/views/survey/Survey.vue"),
    },
    //   {
    //     path: "/about",
    //     name: "about",
    //     // route level code-splitting
    //     // this generates a separate chunk (about.[hash].js) for this route
    //     // which is lazy-loaded when the route is visited.
    //     component: () =>
    //       import(/* webpackChunkName: "about" */ "./views/About.vue"),
    //   },
  ],
});
